import json
import os
import yaml
from tracker import MultiObjectTracker
from collections import defaultdict
from detection_utils import convert_bbox_corners_to_center
from tracking_utils import (
    calculate_direction_and_velocity,
    bbox_image_to_point_cloud,
    direction_vector_image_to_point_cloud,
    convert_velocity,
    calculate_median_velocity,
    convert_to_geojson,
)
import pandas as pd
import numpy as np


# load config
with open("config/tracking_config.yaml", "r") as file:
    config = yaml.safe_load(file)


class Detection:
    def __init__(self, box, score, class_id, feature, detection_uuid, volume, z_center):
        self.box = box
        self.score = score
        self.class_id = class_id
        self.feature = feature
        self.detection_uuid = detection_uuid
        self.volume = volume
        self.z_center = z_center


# load object map
object_map = pd.read_csv(config["object_mapping_path"])

# parameters from config
FRAME_RATE = config["frame_rate"]
USE_DIVIDED = config["use_divided"]
MAP_SIZE = config["map_size"]
SIZE = config[
    "map_size_pixels"
]  # this is the size of one detection image (either full or patch)
NUM_SHARDS = config["num_shards"]
VELOCITY_THRESHOLD = config["velocity_threshold"]

if USE_DIVIDED:
    SIZE = SIZE * NUM_SHARDS

CENTER_X = SIZE / 2
CENTER_Y = SIZE / 2
pixels_per_meter = SIZE / MAP_SIZE

DETECTION_FILE = config["detection_file"]
LOG_SAVE_PATH = config["log_save_path"]
log_file_name = os.path.basename(DETECTION_FILE).replace(".json", "")
log_file_path = f"{LOG_SAVE_PATH}/{log_file_name}_track.json"
os.makedirs(LOG_SAVE_PATH, exist_ok=True)

# load detections
with open(DETECTION_FILE, "r") as file:
    detections = [json.loads(line) for line in file]

# initialize track log files
with open(log_file_path, "w") as file:
    pass

frames = defaultdict(list)
for detection in detections:
    frame_id = detection["frame_id"]
    frames[frame_id].append(detection)

dt = 1 / FRAME_RATE  # fps
tracker = MultiObjectTracker(
    dt=dt,
    tracker_kwargs={"max_staleness": config["tracker_config"]["max_staleness"]},
    model_spec=config["tracker_config"]["model_spec"],
    matching_fn_kwargs=config["tracker_config"]["matching_fn_kwargs"],
)

track_positions = defaultdict(list)  # {track_id: [(x, y)]}
track_times = defaultdict(list)  # {track_id: [time]}
track_ids = []

# track detections per frame
for frame_id in sorted(frames.keys()):
    frame_detections = frames[frame_id]

    # convert to motpy detections
    motpy_detections = []
    detection_to_metadata = {}
    for detection in frame_detections:
        bbox = detection["bbox"]
        score = detection["score"]
        class_id = detection["class_id"]
        detection_uuid = detection["detection_uuid"]
        volume = detection["detection_volume"]
        z_center = detection["detection_z_center_point_cloud"]

        if score > config["detection_threshold"]:
            motpy_detection = Detection(
                box=bbox,
                score=score,
                class_id=class_id,
                feature=None,
                detection_uuid=detection_uuid,
                volume=volume,
                z_center=z_center,
            )
            motpy_detections.append(motpy_detection)

    tracker.step(detections=motpy_detections)

    tracks = tracker.active_tracks()

    timestamp = frame_detections[0]["timestamp"]

    for idx, track in enumerate(tracks):
        track_id = track.id
        bbox = convert_bbox_corners_to_center(track.box)

        if track_id not in track_ids:
            track_positions[track_id] = []
            track_times[track_id] = []
            track_ids.append(track_id)

        track_positions[track_id].append(bbox[:2])  # Store track position
        frame_time = int(frame_id) / FRAME_RATE  # Convert frame ID to seconds
        track_times[track_id].append(frame_time)  # Seconds

        # Get velocities and direction vectors
        direction_vectors, velocities = calculate_direction_and_velocity(
            track_positions[track_id], track_times[track_id]
        )

        if direction_vectors:
            direction_vector = direction_vectors[-1]
            velocity = velocities[-1]
        else:
            direction_vector = (0, 0)
            velocity = 0

        velocity_ms = convert_velocity(velocity, MAP_SIZE, SIZE)
        direction_vector_point_cloud = direction_vector_image_to_point_cloud(
            direction_vector, pixels_per_meter
        )
        bbox_pc_format = bbox_image_to_point_cloud(
            bbox, pixels_per_meter, CENTER_X, CENTER_Y
        )
        class_name = object_map[object_map["id"] == track.class_id][
            "combined_class"
        ].values[0]

        # Retrieve additional metadata using the bounding box of the detection
        additional_metadata = detection_to_metadata.get(tuple(track.box), {})

        # Create log row
        track_log_line = {
            "frame_id": frame_id,
            "timestamp": int(timestamp),
            "track_id": track.id,
            "bbox": list(track.box),
            "bbox_point_cloud": bbox_pc_format,
            "class_id": int(track.class_id),
            "class_name": class_name,
            "direction_vector_point_cloud": direction_vector_point_cloud
            + [track.z_center],
            "velocity_ms": float(velocity_ms),
            "confidence": float(track.score),
            "detection_uuid": track.detection_uuid,
            "detection_volume": float(track.volume),
        }

        with open(log_file_path, "a") as json_file:
            json_file.write(json.dumps(track_log_line) + "\n")

# restructuring the tracks into tracklets
restructured_data = {}
# Read the file line by line and parse each line as a JSON object
with open(log_file_path, "r") as file:
    for line in file:
        entry = json.loads(line.strip())
        track_id = entry["track_id"]
        if track_id not in restructured_data:
            restructured_data[track_id] = []
        restructured_data[track_id].append(
            {
                "frame_id": entry["frame_id"],
                "timestamp": entry["timestamp"],
                "bbox": entry["bbox"],
                "bbox_point_cloud": entry["bbox_point_cloud"],
                "class_id": entry["class_id"],
                "class_name": entry["class_name"],
                "direction_vector_point_cloud": entry["direction_vector_point_cloud"],
                "velocity_ms": entry["velocity_ms"],
                "confidence": entry["confidence"],
                "detection_uuid": entry["detection_uuid"],
                "detection_volume": entry["detection_volume"],
            }
        )

# Filter out tracklets with a median velocity lower than the threshold
filtered_data = {}
for track_id, track_data in restructured_data.items():
    median_velocity = calculate_median_velocity(track_data)
    if median_velocity >= VELOCITY_THRESHOLD:
        filtered_data[track_id] = track_data

# save the restructured data
restructured_file_path = log_file_path.replace("_track", "_tracklets")
with open(restructured_file_path, "w") as file:
    json.dump(restructured_data, file, indent=4)

# filter out tracklets with a median velocity lower than the threshold
filtered_data = {}
for track_id, track_data in restructured_data.items():
    median_velocity = calculate_median_velocity(track_data)
    if median_velocity >= VELOCITY_THRESHOLD:
        filtered_data[track_id] = track_data

# save the filtered data
filtered_file_path = restructured_file_path.replace("_tracklets", "_tracklets_filtered")
with open(filtered_file_path, "w") as file:
    json.dump(filtered_data, file, indent=4)

# GeoJSON conversion matrix and base
with open(config["geo_reference_matrix"], "r") as file:
    geo_transform_matrix = json.load(file)

base = geo_transform_matrix["base"]
geojson_data = convert_to_geojson(filtered_data, base)

# Save GeoJSON to a file
geo_json_path = filtered_file_path.replace(
    "_tracklets_filtered", "_tracklets_filtered_geojson"
)
with open(geo_json_path, "w") as f:
    json.dump(geojson_data, f, indent=2)
