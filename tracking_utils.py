import open3d as o3d
import numpy as np
from scipy.spatial import KDTree
from pyproj import CRS
from pyproj import Transformer
import open3d


def calculate_direction_and_velocity(positions, time_stamps):
    """
    Calculates the direction vectors and velocities between consecutive positions.

    Args:
        positions (list): A list of positions.
        time_stamps (list): A list of time stamps corresponding to the positions.

    Returns:
        tuple: A tuple containing two lists:
            - direction_vectors: A list of direction vectors between consecutive positions.
            - velocities: A list of velocities between consecutive positions.
    """
    direction_vectors = []
    velocities = []

    for i in range(1, len(positions)):
        pos1 = np.array(positions[i - 1])
        pos2 = np.array(positions[i])
        delta_pos = pos2 - pos1

        # direction vector between the positions
        if np.linalg.norm(delta_pos) == 0:
            direction_vector = [0, 0]
        else:
            direction_vector = delta_pos / np.linalg.norm(delta_pos)
        direction_vectors.append(direction_vector)

        # velocity between the positions
        time_elapsed = time_stamps[i] - time_stamps[i - 1]
        distance = np.linalg.norm(delta_pos)
        if time_elapsed == 0:
            velocity = 0
        else:
            velocity = distance / time_elapsed
        velocities.append(velocity)

    return direction_vectors, velocities


def convert_velocity(px_per_sec, map_size, image_size):
    """
    Converts velocity from pixels per second to meters per second.

    Args:
        px_per_sec (float): Velocity in pixels per second.
        map_size (float): Size of the map in meters.
        image_size (int): Size of the image in pixels.

    Returns:
        float: Velocity in kilometers per hour.
    """
    meters_per_pixel = map_size / image_size
    meters_per_sec = px_per_sec * meters_per_pixel
    return meters_per_sec


def bbox_image_to_point_cloud(bbox, pixels_per_meter, img_center_x, img_center_y):
    """
    Converts a bounding box from image coordinates to point cloud coordinates.

    Args:
        bbox (list): The bounding box in the format [y_center, x_center, height, width].
        pixels_per_meter (float): The conversion factor from pixels to meters.
        img_center_x (float): The x-coordinate of the image center.
        img_center_y (float): The y-coordinate of the image center.

    Returns:
        list: The bounding box in point cloud coordinates [x_center_pc, y_center_pc, width_pc, height_pc].
    """

    # bounding box coming from the image is in the format [y_center, x_center, height, width] (axis gets flipped at tracking)
    y_center_img, x_center_img, height_img, width_img = bbox

    x_center_pc = (x_center_img - img_center_x) / pixels_per_meter
    y_center_pc = (y_center_img - img_center_y) / pixels_per_meter

    width_pc = width_img / pixels_per_meter
    height_pc = height_img / pixels_per_meter
    return [x_center_pc, y_center_pc, width_pc, height_pc]


def direction_vector_image_to_point_cloud(direction_vector, scale_factor):
    """
    Converts a direction vector from image coordinates to point cloud coordinates.

    Args:
        direction_vector (tuple): A tuple containing the x and y components of the direction vector in image coordinates.
        scale_factor (float): The scale factor to be applied to the direction vector.
        img_center_x (float): The x-coordinate of the image center.
        img_center_y (float): The y-coordinate of the image center.

    Returns:
        list: A list containing the x and y components of the direction vector in point cloud coordinates.
    """
    dy_img, dx_img = direction_vector  # tracking switches x and y
    dx_pc = dx_img / scale_factor
    dy_pc = dy_img / scale_factor
    return [dx_pc, dy_pc]


def calculate_median_velocity(track_data):
    """
    Calculate the median velocity from a list of track data.

    Parameters:
    track_data (list): A list of dictionaries representing track data. Each dictionary should have a 'velocity_ms' key.

    Returns:
    float: The median velocity in meters per second.
    """
    velocities = [line["velocity_ms"] for line in track_data]
    return np.median(velocities)


def get_distance_scales(options):
    """
    Calculate distance scales based on latitude, longitude, and precision options.

    Args:
        options (dict): A dictionary containing the latitude, longitude, and highPrecision values.

    Returns:
        dict: A dictionary containing the calculated distance scales.

    Raises:
        AssertionError: If latitude or longitude is not finite.

    """

    TILE_SIZE = 512
    EARTH_CIRCUMFERENCE = 40.03e6

    latitude = options["latitude"]
    longitude = options["longitude"]
    highPrecision = options["highPrecision"]
    assert np.isfinite(latitude) and np.isfinite(longitude)

    worldSize = TILE_SIZE
    latCosine = np.cos(np.deg2rad(latitude))

    unitsPerDegreeX = worldSize / 360
    unitsPerDegreeY = unitsPerDegreeX / latCosine

    altUnitsPerMeter = worldSize / EARTH_CIRCUMFERENCE / latCosine

    result = {
        "unitsPerMeter": [altUnitsPerMeter, altUnitsPerMeter, altUnitsPerMeter],
        "metersPerUnit": [
            1 / altUnitsPerMeter,
            1 / altUnitsPerMeter,
            1 / altUnitsPerMeter,
        ],
        "unitsPerDegree": [unitsPerDegreeX, unitsPerDegreeY, altUnitsPerMeter],
        "degreesPerUnit": [
            1 / unitsPerDegreeX,
            1 / unitsPerDegreeY,
            1 / altUnitsPerMeter,
        ],
    }

    if highPrecision:
        latCosine2 = (np.tan(np.deg2rad(latitude))) / latCosine
        unitsPerDegreeY2 = (unitsPerDegreeX * latCosine2) / 2
        altUnitsPerDegree2 = (worldSize / EARTH_CIRCUMFERENCE) * latCosine2
        altUnitsPerMeter2 = (altUnitsPerDegree2 / unitsPerDegreeY) * altUnitsPerMeter

        result["unitsPerDegree2"] = [0, unitsPerDegreeY2, altUnitsPerDegree2]
        result["unitsPerMeter2"] = [altUnitsPerMeter2, 0, altUnitsPerMeter2]

    return result


def world_to_lng_lat(xy):
    """
    Converts world coordinates to longitude and latitude.

    Args:
        xy (tuple): A tuple containing the x and y coordinates.

    Returns:
        list: A list containing the longitude and latitude values.
    """
    PI = np.pi
    PI_4 = PI / 4
    RADIANS_TO_DEGREES = 180 / PI
    TILE_SIZE = 512

    x, y = xy
    lambda2 = (x / TILE_SIZE) * (2 * PI) - PI
    phi2 = 2 * (np.arctan(np.exp((y / TILE_SIZE) * (2 * PI) - PI)) - PI_4)
    return [lambda2 * RADIANS_TO_DEGREES, phi2 * RADIANS_TO_DEGREES]


def lng_lat_to_world(lng_lat):
    """
    Converts longitude and latitude coordinates to world coordinates.

    Args:
        lng_lat (list): A list containing longitude and latitude values.

    Returns:
        list: A list containing the x and y coordinates in the world coordinate system.
    """
    PI = np.pi
    PI_4 = PI / 4
    DEGREES_TO_RADIANS = PI / 180
    TILE_SIZE = 512

    longitude, latitude = lng_lat[:2]
    assert np.isfinite(longitude)
    assert np.isfinite(latitude) and latitude >= -90 and latitude <= 90

    lambda2 = longitude * DEGREES_TO_RADIANS
    phi2 = latitude * DEGREES_TO_RADIANS
    x = (TILE_SIZE * (lambda2 + PI)) / (2 * PI)
    y = (TILE_SIZE * (PI + np.log(np.tan(PI_4 + phi2 * 0.5)))) / (2 * PI)
    return [x, y]


def add_meters_to_lng_lat(lng_lat_z, xyz):
    """
    Adds meters to the given longitude, latitude, and altitude (z) coordinates.

    Args:
        lng_lat_z (list): A list containing the longitude, latitude, and altitude (z) coordinates.
        xyz (list): A list containing the x, y, and z distances to be added to the coordinates.

    Returns:
        list: A list containing the updated longitude, latitude, and altitude (z) coordinates.
    """
    longitude, latitude, z0 = lng_lat_z
    x, y, z = xyz

    distance_scales = get_distance_scales(
        {"longitude": longitude, "latitude": latitude, "highPrecision": True}
    )

    unitsPerMeter = distance_scales["unitsPerMeter"]
    unitsPerMeter2 = distance_scales["unitsPerMeter2"]

    worldspace = lng_lat_to_world(lng_lat_z)
    worldspace[0] += x * (unitsPerMeter[0] + unitsPerMeter2[0] * x)
    worldspace[1] += y * (unitsPerMeter[1] + unitsPerMeter2[1] * y)
    new_lng_lat = world_to_lng_lat(worldspace)
    new_z = (z0 or 0) + (z or 0)
    return [new_lng_lat[0], new_lng_lat[1], new_z]


def transform_crs(x, y, z=None, from_crs="EPSG:4326", to_crs="EPSG:3879"):
    """
    Transforms coordinates from one coordinate reference system (CRS) to another.

    Args:
        x (float): The x-coordinate.
        y (float): The y-coordinate.
        z (float, optional): The z-coordinate. Defaults to None.
        from_crs (str): The source CRS.
        to_crs (str): The target CRS.

    Returns:
        tuple: A tuple containing the transformed coordinates.
    """
    transformer = Transformer.from_crs(from_crs, to_crs)
    if z is None:
        return transformer.transform(x, y)
    else:
        return transformer.transform(x, y, z)


def convert_to_geojson(tracking_data, base):
    """
    Converts tracking data to GeoJSON format.

    Args:
        tracking_data (dict): A dictionary containing tracking data.

    Returns:
        dict: A GeoJSON representation of the tracking data.
    """

    geojson = {"type": "FeatureCollection", "features": []}

    for track_id, points in tracking_data.items():
        coordinates = []
        velocities = []

        for point in points:

            x = float(point["bbox_point_cloud"][0])
            y = float(point["bbox_point_cloud"][1])
            z = 0

            # Transform the coordinates
            lat, lon, nz = add_meters_to_lng_lat(base + [0], [x, y, z])
            coordinates.append([lat, lon, nz])
            velocities.append(point["velocity_ms"])

        # Calculate average velocity
        avg_velocity = sum(velocities) / len(velocities) if velocities else 0

        # Create a GeoJSON feature
        feature = {
            "type": "Feature",
            "properties": {"Velocity": avg_velocity},
            "geometry": {"type": "LineString", "coordinates": coordinates},
            "id": track_id,
        }

        geojson["features"].append(feature)

    return geojson
