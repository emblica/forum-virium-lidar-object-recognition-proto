import yaml
from ouster import client
from ouster import pcap
from contextlib import closing
import numpy as np
from hmap import build_hmap
import sys
import cv2
import os
import json
from detection_utils import (
    divide_image_into_patches,
    get_detections_from_yolo,
    run_inference_on_patch,
    draw_bounding_boxes,
    detection_to_point_cloud,
    extract_points_within_bbox,
    calculate_bounding_box_volume_and_z_center,
    generate_uid,
    convert_bbox_corners_to_center,
)
import open3d as o3d
import pandas as pd
import numpy as np
import super_gradients

# Load configuration from YAML file
with open("config/detection_config.yaml", "r") as f:
    config = yaml.safe_load(f)

USE_DIVIDED = config["use_divided"]
MAP_SIZE = config["map_size"]
SIZE = config["map_size_pixels"]
NUM_SHARDS = config["num_shards"]
TAKE_ONLY_NTH_FRAME = config["take_only_nth_frame"]
RECORDING_LEN = config["recording_len"]
VISUALIZE = config["visualize"]
SAVE_DEPTHMAPS = config["save_depthmaps"]
IMG_OUTPUT_DIR = config["img_output_dir"]
IMG_OUTPUT_DIR_FULL = config["img_output_dir_full"]
PCAP_DATA_DIR = config["pcap_data_dir"]
MODEL_PATH = config["model_path"]
LOG_SAVE_PATH = config["log_save_path"]
metadata_path = config["metadata_path"]
transformation_matrix_path = config["transformation_matrix_path"]
object_mapping_path = config["object_mapping_path"]
SAVE_DETECTION_POINT_CLOUDS = config["save_detection_point_clouds"]
DETECTION_POINT_CLOUD_DIR = config["save_detection_point_cloud_save_path"]

# Multiply size by number of shards if divided
if USE_DIVIDED:
    SIZE = SIZE * NUM_SHARDS

CENTER_X = SIZE / 2
CENTER_Y = SIZE / 2
pixels_per_meter = SIZE / MAP_SIZE

# Reading metadata and pcap files from the local directory
metadata_path = config["metadata_path"]
pcap_name = metadata_path.split("/")[-1].split(".")[0]
pcap_path = f"{PCAP_DATA_DIR}/{pcap_name}.pcap"
detection_point_cloud_save_path = f"{DETECTION_POINT_CLOUD_DIR}/{pcap_name}"

# Load object mapping
object_map = pd.read_csv(object_mapping_path)
object_map.sort_values(by="id", inplace=True)
classes = list(object_map.combined_class.unique())

# Load YOLO model
yolo_model = super_gradients.training.models.get(
    "yolo_nas_s", checkpoint_path=MODEL_PATH, num_classes=len(classes)
)

# GeoJSON conversion matrix and base
with open(transformation_matrix_path, "r") as file:
    geo_transform_matrix = json.load(file)

M = np.array(geo_transform_matrix["M"]).reshape((4, 4))
transformation_matrix = M.T

# Prepare log file
os.makedirs(LOG_SAVE_PATH, exist_ok=True)
log_file = f"{LOG_SAVE_PATH}/{pcap_name}.json"
with open(log_file, "w") as file:
    pass

# Create directories if heightmaps are saved
if SAVE_DEPTHMAPS:
    output_path = f"{IMG_OUTPUT_DIR}/{pcap_name}"
    output_path_full = f"{IMG_OUTPUT_DIR_FULL}/{pcap_name}"
    os.makedirs(output_path, exist_ok=True)
    os.makedirs(output_path_full, exist_ok=True)

# Create directories if detection point clouds are saved
if SAVE_DETECTION_POINT_CLOUDS:
    os.makedirs(detection_point_cloud_save_path, exist_ok=True)

with open(metadata_path, "r") as f:
    info = client.SensorInfo(f.read())
    source = pcap.Pcap(pcap_path, info)

start_timestamp = None
with closing(client.Scans(source)) as scans:
    for i, scan in enumerate(scans):
        if i == 0:
            continue  # skip first frame
        ts_0 = scan.timestamp[0]
        if start_timestamp is None:
            start_timestamp = ts_0
        if i % TAKE_ONLY_NTH_FRAME != 0:
            continue

        # Compute point cloud using client.SensorInfo and client.LidarScan
        xyz = client.XYZLut(info)(scan)
        time_delta = ts_0 - start_timestamp  # in nanoseconds
        # Convert time_delta to seconds
        td_seconds = time_delta / 1000000000
        # Split to minutes and seconds
        minutes = int(td_seconds // 60)
        seconds = int(td_seconds % 60)

        frame_id = scan.frame_id
        # Create point cloud and coordinate axes geometries
        cloud = o3d.geometry.PointCloud(
            o3d.utility.Vector3dVector(xyz.reshape((-1, 3)))
        )

        # Transform point cloud
        cloud2 = cloud.transform(transformation_matrix)

        # Build heightmap
        heightmap = build_hmap(
            np.asarray(cloud2.points).astype("float32"), MAP_SIZE, SIZE
        )

        if USE_DIVIDED:
            patches = divide_image_into_patches(heightmap, NUM_SHARDS)

        # Save depthmaps if needed
        if SAVE_DEPTHMAPS:
            # Save full heightmap separately
            patch_filename = f"{output_path_full}/ts_{minutes}min_{seconds}s_f{frame_id}_n{i}_{pcap_name}.png"
            cv2.imwrite(patch_filename, heightmap)
            if USE_DIVIDED:
                for patch, row, col in patches:
                    patch_filename = f"{output_path}/ts_{minutes}min_{seconds}s_f{frame_id}_n{i}_r{row}_c{col}_{pcap_name}.png"
                    cv2.imwrite(patch_filename, patch)

        # Detect and log objects from patches
        if USE_DIVIDED:
            detections = []
            for patch, row, col in patches:
                detections.extend(
                    run_inference_on_patch(patch, yolo_model, (row, col), len(classes))
                )
        else:
            detections = get_detections_from_yolo(
                heightmap, yolo_model, num_classes=len(classes)
            )

        # Log and extract detections
        for detection in detections:
            # convert detection corner box to center xy, width and height
            bbox = convert_bbox_corners_to_center(detection.box)

            # convert detection box to point cloud coordinates
            bbox_pc = detection_to_point_cloud(
                bbox, pixels_per_meter, CENTER_X, CENTER_Y
            )

            # extract detection point cloud
            detection_cloud, bbox_o3d_format = extract_points_within_bbox(
                cloud2, bbox_pc, outlier_removal=True
            )

            # get volume and center z of the bounding box
            detection_cloud_points = np.asarray(detection_cloud.points)

            # avoid empty point clouds
            if len(detection_cloud_points) == 0:
                continue

            volume, z_center = calculate_bounding_box_volume_and_z_center(
                detection_cloud_points, bbox_o3d_format
            )
            detection_uuid = generate_uid()

            detection_log = {
                "frame_id": int(frame_id),
                "time_delta_seconds": float(td_seconds),
                "timestamp": int(time_delta),
                "detection_uuid": detection_uuid,
                "bbox": detection.box,
                "bbox_pc": bbox_pc,
                "score": float(detection.score),
                "class_id": int(detection.class_id),
                "detection_volume": float(volume),
                "detection_z_center_point_cloud": float(z_center),
            }

            # save detection to log
            with open(log_file, "a") as json_file:
                json_file.write(json.dumps(detection_log) + "\n")

            # save detection point cloud
            if SAVE_DETECTION_POINT_CLOUDS:
                detection_point_cloud_filename = (
                    f"{detection_point_cloud_save_path}/{detection_uuid}.pcd"
                )
                o3d.io.write_point_cloud(
                    detection_point_cloud_filename, detection_cloud
                )

        print(f'Frame {frame_id} processed ({minutes} minutes).')

        # visualization for debugging if needed
        if VISUALIZE:
            heightmap_with_boxes = draw_bounding_boxes(heightmap, detections)
            heightmap_with_boxes = heightmap_with_boxes.astype(np.uint8)
            cv2.imshow("Detections", heightmap_with_boxes)
            cv2.waitKey(1)

        # stop when recording length is reached
        if td_seconds >= RECORDING_LEN:
            break

if VISUALIZE:
    cv2.destroyAllWindows()
