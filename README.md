# Forum Virium Lidar Object Recognition

![image1](img/image1.png)

## Installation

Create Python environment. Recommended Python version 3.11 or lower.

To install the required Python packages, run the following commands in your terminal:

```zsh
pip install --upgrade pip
pip install -r requirements.txt
pip install super-gradients --no-deps
```

### How to run

1. Generate transformation matrix for the point cloud to align it and save the matrix in JSON format to. Take the transformation matrix from [Georeference tool](https://georef.apps.emblica.com).

2. Get detection model from [Google Drive folder](https://drive.google.com/drive/folders/1uwIqkAiNwjE1WClSOvUExbjB-FqJIkGU?usp=sharing) and save to desired location.

3. Open `config/detection_config.yaml` and update parameters to fetch and save data to correct locations. Note that `metadata_path` determines the pcap file path as well. You can also determine the length of the detection and tracking recording from same configuration file. 

4. Activate pre-installed Python environment
```zsh
pyenv activate <your pyenv>
```

5. Run detection
```zsh
python 01_create_map_and_detect.py
```
6. Open `config/tracking_config.yaml` and update the parameters to fetch data from correct locations.

7. Run tracking
```zsh
python 02_track_detections.py
```
8. Find results in desired locations defined in configuration files.

## Contents

### 01_create_map_and_detect.py

This script takes the pcap file specified in `config/detection_config.yaml` (metadata path), creates heightmaps of the frames, and detects objects in each frame. The transformation matrix is provided in a separate JSON file (location defined in the config). It produces a `detection_logs/<pcap file name>.json` file containing the detections and extracts the detections from point clouds with UUIDs.

Detection model has been trained with height maps divided into four 640 x 640 pixel patches, inference works best with number of shards being 2, image size 640 and 120 meters.

#### Sample detection
![image2](img/image2.png)

### 02_track_detections.py

This script takes the previous detection log and creates tracking between object bounding boxes. The `tracker.py` file contains the motpy tracker with minor customizations for this project's specific needs. The program produces various track logs:

- `<pcap file name>_track.json`: Objects assigned to their corresponding tracks in each frame. Includes information in point cloud coordinates and meters (some information in pixels as well).
- `<pcap file name>_tracklets.json` / `<pcap file name>_tracklets_filtered.json`: Object detections grouped by tracking IDs, with the filtered version excluding static objects. Includes information in point cloud coordinates and meters (some information in pixels as well).
- `<pcap file name>_tracklets_geojson.json`: Moving object tracks in GeoJSON format and global coordinates.

### Utilities

- `detection_utils.py`: Contains documented functions utilized by the detection program.
- `tracking_utils.py`: Contains documented functions utilized by the tracking program.
- `tracker.py`: The motpy tracking algorithm customized for this pipeline's needs.