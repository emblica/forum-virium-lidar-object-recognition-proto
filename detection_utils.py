import super_gradients
import numpy as np
import re
import cv2
import uuid
import open3d as o3d


def get_class_name(class_id, object_map):
    """
    Returns the class name corresponding to the given class ID.

    Parameters:
    class_id (int): The ID of the class.
    object_map (pandas.DataFrame): The object map containing class IDs and corresponding class names.

    Returns:
    str: The class name corresponding to the given class ID.
    """
    return object_map[object_map["id"] == class_id]["combined_class"].item()


class Detection:
    """
    Represents a detected object.

    Attributes:
        box (tuple): The bounding box coordinates of the object.
        score (float): The confidence score of the detection.
        class_id (int): The class ID of the detected object.
        feature (str): The feature description of the object.
    """

    def __init__(self, box, score, class_id, feature):
        self.box = box
        self.score = score
        self.class_id = class_id
        self.feature = feature


def get_detections_from_yolo(
    img, model, confidence_threshold=0.5, iou_threshold=0.5, num_classes=None
):
    """
    Get object detections from a YOLO model.

    Args:
        img: The input image for object detection.
        confidence_threshold: The confidence threshold for object detection (default: 0.5).
        iou_threshold: The intersection over union threshold for object detection (default: 0.5).
        model_path: The path to the YOLO model checkpoint (default: None).
        num_classes: The number of classes in the YOLO model (default: None).

    Returns:
        detections: A list of Detection objects, each containing the bounding box coordinates, confidence score, class ID, and feature.

    """

    results = model.predict(img, conf=confidence_threshold, iou=iou_threshold)
    prediction = results.prediction
    bboxes = prediction.bboxes_xyxy
    labels = prediction.labels
    confidence_scores = prediction.confidence

    detections = []
    for label, box, conf in zip(labels, bboxes, confidence_scores):
        x1, y1, x2, y2 = box
        feature = None
        detection = Detection(
            box=[x1, y1, x2, y2], score=conf, class_id=label, feature=feature
        )
        detections.append(detection)
    return detections


def convert_bbox_corners_to_center(bbox):
    """
    Converts the bounding box corners to center format.

    Args:
        bbox (list): A list of four values representing the coordinates of the bounding box corners in the order [x_min, y_min, x_max, y_max].

    Returns:
        list: A list of four values representing the coordinates of the bounding box center and its dimensions in the order [x_center, y_center, width, height].
    """
    x_min, y_min, x_max, y_max = bbox
    x_center = (x_min + x_max) / 2
    y_center = (y_min + y_max) / 2
    width = x_max - x_min
    height = y_max - y_min
    return [x_center, y_center, width, height]


def divide_image_into_patches(image, num_patches):
    """
    Divides an image into patches of equal size.

    Args:
        image (numpy.ndarray): The input image.
        num_patches (int): The number of patches to divide the image into.

    Returns:
        list: A list of tuples, where each tuple contains a patch of the image along with its row and column indices.

    """
    patch_size = image.shape[0] // num_patches
    patches = []
    for i in range(num_patches):
        for j in range(num_patches):
            patch = image[
                i * patch_size : (i + 1) * patch_size,
                j * patch_size : (j + 1) * patch_size,
            ]
            patches.append((patch, i, j))
    return patches


def adjust_detections_for_patch(detections, patch_index, patch_size):
    """
    Adjusts the bounding box coordinates of detections based on the patch index and patch size.

    Args:
        detections (list): List of Detection objects representing the original detections.
        patch_index (tuple): Tuple (i, j) representing the patch index.
        patch_size (int): Size of the patch.

    Returns:
        list: List of adjusted Detection objects with updated bounding box coordinates.
    """
    i, j = patch_index
    adjusted_detections = []
    for det in detections:
        x1, y1, x2, y2 = det.box
        x1 += j * patch_size
        y1 += i * patch_size
        x2 += j * patch_size
        y2 += i * patch_size
        adjusted_detections.append(
            Detection(
                box=[x1, y1, x2, y2],
                score=det.score,
                class_id=det.class_id,
                feature=det.feature,
            )
        )
    return adjusted_detections


def run_inference_on_patch(img, model, patch_index, num_classes):
    """
    Runs inference on a patch of an image using a pre-trained model.

    Args:
        img (numpy.ndarray): The input image.
        patch_index (int): The index of the patch within the image.
        num_classes (int): The number of classes in the model.
        model_path (str): The path to the pre-trained model.

    Returns:
        numpy.ndarray: The adjusted detections from patch to full heightmap.

    """
    detections = get_detections_from_yolo(img, model, num_classes=num_classes)
    adjusted_detections = adjust_detections_for_patch(
        detections, patch_index, img.shape[0]
    )
    return adjusted_detections


def collect_patches(image_folder):
    """
    Collects patches from the given image folder.

    Args:
        image_folder (str): The path to the folder containing the images.

    Returns:
        dict: A nested dictionary containing the collected patches.
              The structure of the dictionary is as follows:
              {
                  frame_id: {
                      row: {
                          col: image_file
                      }
                  }
              }
    """
    image_files = sorted([f for f in os.listdir(image_folder) if f.endswith(".png")])
    patches = {}
    filename_pattern = re.compile(
        r"ts_(\d+)min_(\d+)s_f(\d+)_n(\d+)_r(\d+)_c(\d+)_(.*)\.png"
    )
    for image_file in image_files:
        match = filename_pattern.match(image_file)
        if match:
            frame_id = match.group(3)
            row = int(match.group(5))
            col = int(match.group(6))
            if frame_id not in patches:
                patches[frame_id] = {}
            if row not in patches[frame_id]:
                patches[frame_id][row] = {}
            patches[frame_id][row][col] = image_file
    return patches


def draw_bounding_boxes(heightmap, detections):
    """
    Draw bounding boxes on a heightmap image based on the given detections.

    Args:
        heightmap (numpy.ndarray): The heightmap image to draw on.
        detections (list): A list of detections, where each detection is a tuple (x, y, w, h).

    Returns:
        numpy.ndarray: The heightmap image with bounding boxes drawn on it.
    """
    for detection in detections:
        x, y, w, h = map(int, detection.box)
        cv2.rectangle(heightmap, (x, y), (w, h), (0, 255, 0), 2)
    return heightmap


def generate_uid():
    """
    Generates a unique identifier using UUID version 4.

    Returns:
        str: A string representation of the generated UUID.
    """
    return str(uuid.uuid4())


def remove_outliers_statistical(cropped_pc, nb_neighbors=20, std_ratio=2.0):
    """
    Remove statistical outliers from a point cloud.

    Parameters:
    - cropped_pc (PointCloud): The input point cloud.
    - nb_neighbors (int): The number of neighbors to consider for outlier removal. Default is 20.
    - std_ratio (float): The standard deviation ratio threshold for outlier removal. Default is 2.0.

    Returns:
    - filtered_pcd (PointCloud): The filtered point cloud with statistical outliers removed.
    """
    cl, ind = cropped_pc.remove_statistical_outlier(
        nb_neighbors=nb_neighbors, std_ratio=std_ratio
    )
    filtered_pcd = cropped_pc.select_by_index(ind)
    return filtered_pcd


def extract_points_within_bbox(point_cloud, bbox, outlier_removal=True):
    """
    Extracts the points within a specified bounding box from a given point cloud.

    Parameters:
    - point_cloud: The input point cloud to extract points from.
    - bbox: The bounding box coordinates in the format [x_center, y_center, width, height].

    Returns:
    - cropped_pc: The cropped point cloud containing only the points within the bounding box.
    - bbox_converted: The converted bounding box coordinates in the format [x_min, y_min, z_min, x_max, y_max, z_max].
    """

    # Convert bbox to point cloud coordinates, z inf as x,y are from 2D projection image
    x_center, y_center, width, height = bbox
    bbox_converted = [
        x_center - width / 2,
        y_center - height / 2,
        -np.inf,
        x_center + width / 2,
        y_center + height / 2,
        np.inf,
    ]

    # Create bounding box
    bbox_o3d = o3d.geometry.AxisAlignedBoundingBox(
        min_bound=bbox_converted[:3], max_bound=bbox_converted[3:]
    )

    cropped_pc = point_cloud.crop(bbox_o3d)

    if outlier_removal:
        filtered_pc = remove_outliers_statistical(cropped_pc)
        return filtered_pc, bbox_converted
    else:
        return cropped_pc, bbox_converted


def detection_to_point_cloud(bbox, pixels_per_meter, img_center_x, img_center_y):
    """
    Converts a bounding box from image coordinates to point cloud coordinates.

    Args:
        bbox (tuple): A tuple containing the coordinates of the bounding box in the image.
                      The tuple should be in the format (x_center_img, y_center_img, height_img, width_img).
        pixels_per_meter (float): The conversion factor from pixels to meters.
        img_center_x (float): The x-coordinate of the center in image.
        img_center_y (float): The y-coordinate of the center in image.

    Returns:
        list: A list containing the coordinates of the bounding box in the point cloud.
              The list is in the format [x_center_pc, y_center_pc, width_pc, height_pc].
    """
    y_center_img, x_center_img, height_img, width_img = bbox

    x_center_pc = (x_center_img - img_center_x) / pixels_per_meter
    y_center_pc = (y_center_img - img_center_y) / pixels_per_meter

    width_pc = width_img / pixels_per_meter
    height_pc = height_img / pixels_per_meter
    return [x_center_pc, y_center_pc, width_pc, height_pc]


def calculate_bounding_box_volume_and_z_center(points, bbox_o3d_format):
    """
    Calculate the volume and z center of a bounding box.

    Parameters:
    - points: numpy array, representing the point cloud
    - bbox_o3d_format: numpy array, representing the bounding box in Open3D format

    Returns:
    - volume: float, the volume of the adjusted bounding box
    - z_center: float, the z-coordinate of the center of the adjusted bounding box
    """
    min_z = np.min(points[:, 2])
    max_z = np.max(points[:, 2])
    z_center = (min_z + max_z) / 2

    # open3d bounding box from the extraction
    bbox_min = bbox_o3d_format[:3]
    bbox_max = bbox_o3d_format[3:]

    # use the old bounding box x,y and get the z from the point cloud
    adjusted_bbox_min = [bbox_min[0], bbox_min[1], min_z]
    adjusted_bbox_max = [bbox_max[0], bbox_max[1], max_z]

    # calculate box volume
    adjusted_bbox = o3d.geometry.AxisAlignedBoundingBox(
        min_bound=adjusted_bbox_min, max_bound=adjusted_bbox_max
    )
    volume = np.prod(adjusted_bbox.get_extent())

    return volume, z_center
